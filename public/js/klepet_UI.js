/**
 * Zamenjava kode smeška s sliko (iz oddaljenega strežnika
 * https://sandbox.lavbic.net/teaching/OIS/gradivo/{smesko}.png)
 * 
 * @param vhodnoBesedilo sporočilo, ki lahko vsebuje kodo smeška
 */
function dodajSmeske(vhodnoBesedilo) {
  var preslikovalnaTabela = {
    ";)": "wink.png",
    ":)": "smiley.png",
    "(y)": "like.png",
    ":*": "kiss.png",
    ":(": "sad.png"
  }
  for (var smesko in preslikovalnaTabela) {
    vhodnoBesedilo = vhodnoBesedilo.split(smesko).join(
      "<img src='https://sandbox.lavbic.net/teaching/OIS/gradivo/" +
      preslikovalnaTabela[smesko] + "' />");
  }
  return vhodnoBesedilo;
}


/**
 * Čiščenje besedila sporočila z namenom onemogočanja XSS napadov
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementEnostavniTekst(sporocilo) {
  var jeSmesko = sporocilo.indexOf("https://sandbox.lavbic.net/teaching/OIS/gradivo/") > -1;
  if (jeSmesko) {
    sporocilo = 
      sporocilo.split("<").join("&lt;")
               .split(">").join("&gt;")
               .split("&lt;img").join("<img")
               .split("png' /&gt;").join("png' />");
    return divElementHtmlTekst(sporocilo);
  } else {
    return $('<div style="font-weight: bold"></div>').text(sporocilo);  
  }
}


/**
 * Prikaz "varnih" sporočil, t.j. tistih, ki jih je generiral sistem
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}


/**
 * Obdelaj besedilo, ki ga uporabnik vnese v obrazec na spletni strani, kjer
 * je potrebno ugotoviti ali gre za ukaz ali za sporočilo na kanal
 * 
 * @param klepetApp objekt Klepet, ki nam olajša obvladovanje 
 *        funkcionalnosti uporabnika
 * @param socket socket WebSocket trenutno prijavljenega uporabnika
 */
function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  sporocilo = dodajSmeske(sporocilo);
  var sistemskoSporocilo;
  
  // Če uporabnik začne sporočilo z znakom '/', ga obravnavaj kot ukaz
  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      for(var j = 0; j < vzdevekZaIme.length; j++) {
        sistemskoSporocilo = sistemskoSporocilo.replace(new RegExp(vzdevekZaIme[j].ime, "g"), vzdevekZaIme[j].vzdevek + " (" + vzdevekZaIme[j].ime + ")");
      }
      $('#sporocila').append(zaznajSlike(divElementHtmlTekst(sistemskoSporocilo)));
    }
  // Če gre za sporočilo na kanal, ga posreduj vsem članom kanala
  } else {
    sporocilo = filtirirajVulgarneBesede(sporocilo);
    klepetApp.posljiSporocilo(trenutniKanal, sporocilo);
    $('#sporocila').append(zaznajSlike(divElementEnostavniTekst(sporocilo)));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

// Branje vulgarnih besed v seznam
var vulgarneBesede = [];
$.get('/swearWords.txt', function(podatki) {
  vulgarneBesede = podatki.split('\r\n'); 
});

/**
* Iz podanega niza vse besede iz seznama vulgarnih besed zamenjaj z enako dolžino zvezdic (*).
* 
* @param vhodni niz
*/
function filtirirajVulgarneBesede(vhod) {

 for (var i in vulgarneBesede) {
   var re = new RegExp('\\b' + vulgarneBesede[i] + '\\b', 'gi');
   vhod = vhod.replace(re, "*".repeat(vulgarneBesede[i].length));
 }
 
 return vhod;
}


function zaznajSlike(element) {
  var text = element.html();
  var urls = text.match(/http\S*\.(jpg|png|gif)/g);

  if(!urls) return element;

  for(var i = 0; i < urls.length; i++) {
    if(urls[i].indexOf("https://sandbox.lavbic.net/teaching/OIS/gradivo/") > -1) continue
    text += "<br/><img style=\"margin:20px; width:200px;\" src=\"" + urls[i] + "\">";
  }

  element.html(text);
  return element;
}

var socket = io.connect();
var trenutniVzdevek = "";
var trenutniKanal = "";
var vzdevekZaIme = [];

// Poačakj, da se celotna stran naloži, šele nato začni z izvajanjem kode
$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  
  // Prikaži rezultat zahteve po spremembi vzdevka
  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      trenutniVzdevek = rezultat.vzdevek;
      $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });
  
  // Prikaži rezultat zahteve po spremembi kanala
  socket.on('pridruzitevOdgovor', function(rezultat) {
    trenutniKanal = rezultat.kanal;
    $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });
  
  // Prikaži prejeto sporočilo
  socket.on('sporocilo', function (sporocilo) {
    for(var j = 0; j < vzdevekZaIme.length; j++) {
      sporocilo.besedilo = sporocilo.besedilo.replace(new RegExp(vzdevekZaIme[j].ime), vzdevekZaIme[j].vzdevek + " (" + vzdevekZaIme[j].ime + ")");
    }
    var novElement = divElementEnostavniTekst(sporocilo.besedilo);
    if(sporocilo.besedilo.indexOf("&#9756;") > -1 && sporocilo.vzdevek) {
      novElement.html(novElement.html().replace("&amp;#9756;", "&#9756;"));
    }
    $('#sporocila').append(zaznajSlike(novElement));
  });
  
  // Prikaži seznam kanalov, ki so na voljo
  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var i in kanali) {
      if (kanali[i] != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanali[i]));
      }
    }
  
    // Klik na ime kanala v seznamu kanalov zahteva pridružitev izbranemu kanalu
    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  
  // Prikaži seznam trenutnih uporabnikov na kanalu
  socket.on('uporabniki', function(uporabniki) {
    $('#seznam-uporabnikov').empty();
    for (var i=0; i < uporabniki.length; i++) {
      for(var j = 0; j < vzdevekZaIme.length; j++) {
        uporabniki[i] = uporabniki[i].replace(new RegExp(vzdevekZaIme[j].ime, "g"), vzdevekZaIme[j].vzdevek + " (" + vzdevekZaIme[j].ime + ")");
      }
      var element = divElementEnostavniTekst(uporabniki[i]);
      if(element.text() != trenutniVzdevek) {
        element.click(function() {
          var user = $(event.target).text();
          for(var j = 0; j < vzdevekZaIme.length; j++) {
            if(user == (vzdevekZaIme[j].vzdevek + " (" + vzdevekZaIme[j].ime + ")")) {
              user = vzdevekZaIme[j].ime;
            }
          }
          var sporocilo = posljiKrc(user);
          $('#sporocila').append(divElementHtmlTekst(sporocilo));
          $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
          $('#poslji-sporocilo').focus();
        });
      }
      
      $('#seznam-uporabnikov').append(element);

    }
  });
  
  socket.on('bcVzdevekSprememba', function(rezultat) {
    for(var i = 0; i < vzdevekZaIme.length; i++) {
      if(vzdevekZaIme[i].ime == rezultat.starVzdevek) vzdevekZaIme[i].ime = rezultat.novVzdevek;
    }
  });
  
  // Seznam kanalov in uporabnikov posodabljaj vsako sekundo
  setInterval(function() {
    socket.emit('kanali');
    socket.emit('uporabniki', {kanal: trenutniKanal});
  }, 1000);

  $('#poslji-sporocilo').focus();
  
  // S klikom na gumb pošljemo sporočilo strežniku
  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});

function spremeniVzdevek(uporabnik, vzdevek) {
  var obj = {};
  
  for(var i = 0; i < vzdevekZaIme.length; i++) {
    if(vzdevekZaIme[i].ime == uporabnik) vzdevekZaIme.splice(i, 1);
  }
  
  obj.vzdevek=vzdevek;
  obj.ime = uporabnik;
  vzdevekZaIme.push(obj);
}

function posljiKrc(uporabnik) {
  this.socket.emit('sporocilo', {vzdevek: uporabnik, besedilo: "&#9756;"});
  for(var j = 0; j < vzdevekZaIme.length; j++) {
    if(vzdevekZaIme[j].ime == uporabnik) uporabnik = vzdevekZaIme[j].vzdevek + " (" + vzdevekZaIme[j].ime + ")";
  }
  return '(zasebno za ' + uporabnik + '): ' + "&#9756;";
};